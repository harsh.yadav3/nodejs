import * as express from "express";
import { AppDataSource as orm } from "../data-source"
import { Cats } from "../entity/Cats";



const route = express.Router();


route.get("/",async(req, res) =>{
    const {age_lte,age_gte}=req.query;
    const query =  await Cats.createQueryBuilder().select("cat").from(Cats,"cat").where("cat.age>=:age_gte AND cat.age<=:age_lte ",{age_gte,age_lte}).getMany();
    res.send(query)
})


module.exports = route