import * as express from "express";
import { AppDataSource as orm } from "../data-source"
import { Cats } from "../entity/Cats";
import { User } from "../entity/User";

const route = express.Router();

// const Cats = new Cats();

route.get('/', async(req, res) =>{
    try{ 
        const users =  await Cats.find()
        res.send(users);
    }
    catch(err){res.send("errorrrrrr!!!!!!!")}
}
);

route.post("/",async(req, res) =>{
    const data = req.body;
    
    let x = Cats.create( data);
    try{
        let re =  await Cats.save(x);
        res.send(re);
    }
    catch(err){res.send(err);}
})

route.get("/:id",async(req, res) =>{
    const {id}=req.params;
    let re = await Cats.findOneBy({"id":id});
    res.send(re)
})


route.patch("/:id",async(req, res) =>{
    const {id}=req.params;
    const data = req.body;
    let datatobeupdated = await Cats.findOneBy({"id":id});
    try{
        let response = Cats.save({...datatobeupdated,...data})
        res.send("updated")
    }
    catch(err){
        res.send("failed")
    }
})

route.delete("/:id",async(req, res) =>{
    const {id}=req.params;
    let re = await Cats.findOneBy({"id":id});
    let rem = await Cats.remove(re);
    res.send(rem)
})


module.exports = route;

// {
//     "id": "03521f13-ca2a-4b6c-b050-78ff243c89a0",
//     "name": "mewo1",
//     "breed": "somebeed",
//     "age": 10,
//     "createdAt": "2022-05-25T13:12:06.620Z",
//     "updatedAt": "2022-05-25T13:12:06.620Z"
//   },