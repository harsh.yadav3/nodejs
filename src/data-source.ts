import "reflect-metadata"
import { DataSource } from "typeorm"
import { Cats } from "./entity/Cats"
import { User } from "./entity/User"

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "admin",
    password: "harshyadav",
    database: "testusers",
    synchronize: true,
    logging: false,
    entities: [User,Cats],
    migrations: [],
    subscribers: [],
})
