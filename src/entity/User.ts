import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: number

    @Column()
    Name: string

    @Column()
    lastName: string
   
    @Column()
    password: string
    
    @Column()
    age: number

}
